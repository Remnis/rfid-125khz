# RFID-125kHz #
Connecting a RDM6300 RFID-Reader/Writer to an Arduino

(c) 2017, Agis Wichert

https://www.youtube.com/watch?v=52xt6LfU9tE


                                      +-----+
         +----[PWR]-------------------| USB |--+
         |                            +-----+  |
         |         GND/RST2  [ ][ ]            |
         |       MOSI2/SCK2  [ ][ ]  A5/SCL[ ] |   . 
         |          5V/MISO2 [ ][ ]  A4/SDA[ ] |   . 
         |                             AREF[ ] |
         |                              GND[ ] |
         | [ ]N/C                    SCK/13[ ] |   .
         | [ ]IOREF                 MISO/12[ ] |   .
         | [ ]RST                   MOSI/11[ ]~|   .
         | [ ]3V3    +---+               10[ ]~|   .
         | [ ]5v    -| A |-               9[ ]~|   .
         | [ ]GND   -| R |-               8[ ] |  .
         | [ ]GND   -| D |-                    |
         | [ ]Vin   -| U |-               7[ ] |   .
         |          -| I |-               6[ ]~|   .
         | [ ]A0    -| N |-               5[ ]~|   .
         | [ ]A1    -| O |-               4[ ] |   .
         | [ ]A2     +---+           INT1/3[X]~|   TX on RDM6300 
         | [ ]A3                     INT0/2[X] |   RX on RDM6300 
         | [ ]A4/SDA  RST SCK MISO     TX>1[ ] |   .
         | [ ]A5/SCL  [ ] [ ] [ ]      RX<0[ ] |   .
         |            [ ] [ ] [ ]              |
         |  UNO_R3    GND MOSI 5V  ____________/
          \_______________________/